#include <EventBookkeeperTools/BookkeeperTool.h>
#include <EventBookkeeperTools/BookkeeperDumperTool.h>
#include <EventBookkeeperTools/CutFlowSvc.h>

#include "../AllExecutedEventsCounterAlg.h"
#include "../EventCounterAlg.h"
#include "../SkimDecisionMultiFilter.h"
#include "../TestFilterReentrantAlg.h"

DECLARE_COMPONENT( AllExecutedEventsCounterAlg )
DECLARE_COMPONENT( BookkeeperDumperTool )
DECLARE_COMPONENT( BookkeeperTool )
DECLARE_COMPONENT( CutFlowSvc )
DECLARE_COMPONENT( EventCounterAlg )
DECLARE_COMPONENT( SkimDecisionMultiFilter )
DECLARE_COMPONENT( TestFilterReentrantAlg )
