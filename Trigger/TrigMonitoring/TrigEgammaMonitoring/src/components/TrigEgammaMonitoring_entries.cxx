#include "../TrigEgammaMonitorBaseAlgorithm.h"
#include "../TrigEgammaMonitorAnalysisAlgorithm.h"
#include "../TrigEgammaMonitorElectronAlgorithm.h"
#include "../TrigEgammaMonitorPhotonAlgorithm.h"
#include "../TrigEgammaMonitorTagAndProbeAlgorithm.h"
#include "../egammaMonitorElectronAlgorithm.h"
#include "../egammaMonitorPhotonAlgorithm.h"

DECLARE_COMPONENT(TrigEgammaMonitorBaseAlgorithm)
DECLARE_COMPONENT(TrigEgammaMonitorAnalysisAlgorithm)
DECLARE_COMPONENT(TrigEgammaMonitorElectronAlgorithm)
DECLARE_COMPONENT(TrigEgammaMonitorPhotonAlgorithm)
DECLARE_COMPONENT(TrigEgammaMonitorTagAndProbeAlgorithm)
DECLARE_COMPONENT(egammaMonitorElectronAlgorithm)
DECLARE_COMPONENT(egammaMonitorPhotonAlgorithm)

